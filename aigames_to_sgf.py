#!/bin/python
#
# Copyright 2016 Jude Hungerford
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import board
import sys
import traceback

class Move:

    def __init__(self, player, row, col):
        self.player = player
        self.row = row
        self.col = col

class Kifu:

    def __init__(self):
        self.width = 0
        self.height = 0
        self.moves = []
        self.board = None
        self.next_player = board.PLAYER1
        self.turn = 0

    def init_board(self, width, height):
        self.width = width
        self.height = height
        self.board = board.Board(width, height)

    def update_next_player(self):
        if self.next_player == board.PLAYER1:
            self.next_player = board.PLAYER2
        else: 
            self.next_player = board.PLAYER1

    def add_move(self, move):
        self.moves.append(move)
        if not self.board.is_legal(move.player, move.row, move.col):
            print("WARNING: illegal move added: turn " + str(self.turn) + " move " + str(move.player) + " " + str(move.row) + " " + str(move.col))
        self.board.place_move(move.player, move.row, move.col)
        self.update_next_player()

    def pass_turn(self):
        self.moves.append(None)
        self.update_next_player()

    def deduce_move(self, data):
        tboard = board.Board(self.width, self.height)
        tboard.parse(data)
        new_move = self.board.find_added_stone(tboard.cell)
        if not new_move == None:
            player, row, col = new_move
            move = Move(player, row, col)
            if player != self.next_player:
                print("WARNING: found stone not from expected player: " + str ((player, row, col)))
            self.add_move(move)
        elif self.turn > 1:
            self.pass_turn()

    def parse(self, data):
        for line in data:
            line = line.strip().lower()
            tokens = line.split()
            if len(tokens) > 0:
                if tokens[0] == "settings":
                    if tokens[1] == "field_width":
                        self.width = int(tokens[2])
                        if self.height > 0:
                            self.init_board(self.width, self.height)
                    elif tokens[1] == "field_height":
                        self.height = int(tokens[2])
                        if self.width > 0:
                            self.init_board(self.width, self.height)
                if tokens[0] == "update" and tokens[1] == "game":
                    if tokens[2] == "field":
                        #print(tokens)
                        self.deduce_move(tokens[3])
                    elif tokens[2] == "move":
                        self.turn = int(tokens[3])
                if tokens[0] == "output":
                    #print(tokens)
                    if tokens[1] == "from" and tokens[2] == "your" and tokens[3] == "bot:":
                        if tokens[4] == "\"place_move":
#                            print(tokens)
                            col = int(tokens[5])
                            row = int(tokens[6].strip('"'))
                            move = Move(self.next_player, row, col) 
                            self.add_move(move)
                        elif tokens[4].strip('"') == 'pass':
                            self.pass_turn()
                            

    def output_strings(self):
        for move in self.moves:
            if not move == None:
                print((move.player, move.row, move.col))
            else:
                print("pass")

    def player_to_letter(self, player):
        if player == board.PLAYER1:
            return 'B'
        else:
            return 'W'

    def int_to_letter(self, i):
        return chr(i + 97)

    def move_to_sgf(self, move):
        if move:
            player = self.player_to_letter(move.player)
            col = self.int_to_letter(move.col)
            row = self.int_to_letter(move.row)
        else:
            player = self.player_to_letter(self.next_player)
            col = ''
            row = ''
        result = ''
        result += ';'
        result += player
        result += '['
        result += col
        result += row
        result += ']'
        return result

    def output_sgf(self):
        self.next_player = board.PLAYER1
        lines = []
        lines.append('(;GM[1]FF[4]CA[UTF-8]AP[AIGamesToSgf]SZ[' + str(self.width) + ']')
        lines.append('PW[player2]PB[player1]')
        for move in self.moves:
            to_add = self.move_to_sgf(move)
            lines.append(to_add)
            self.update_next_player()
        lines.append(')')
        lines.append('')
        print ('\n'.join(lines))
            

kifu = Kifu()
not_finished = True
data = []
count = 0

filename = sys.argv[1]
fp = open(filename, 'r')
for line in fp:
    data.append(line.rstrip('\r\n'))

kifu.parse(data)
kifu.output_sgf()

